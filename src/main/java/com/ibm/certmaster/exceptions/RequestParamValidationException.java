package com.ibm.certmaster.exceptions;

public class RequestParamValidationException extends Exception {
    public RequestParamValidationException(String message) {
        super(message);
    }
}
